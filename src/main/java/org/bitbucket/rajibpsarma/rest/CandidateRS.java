package org.bitbucket.rajibpsarma.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class CandidateRS {
	@GetMapping("/candidates")
	public ResponseEntity<Object> getCandidatesStatus() {
		System.out.println("--- Getting Candidate related status from REST ...");
		Map<String, String> data  = new HashMap<String, String>();
		
		data.put("candidatesUploaded", "5678");
		data.put("totalCandidates", "200000");
		data.put("startDate", new Date().toString());
		data.put("pausedDate", new Date().toString());
		
		//return new ResponseEntity<Object>(data, HttpStatus.OK);

		// CORS related
		HttpHeaders responseHeaders = new HttpHeaders();
    		responseHeaders.set("Access-Control-Allow-Origin", "*");
		return ResponseEntity.ok()
			.headers(responseHeaders)
      			.body(data);
	}
}
