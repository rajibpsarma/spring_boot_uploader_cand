package org.bitbucket.rajibpsarma.cand.vo;

import org.bitbucket.rajibpsarma.common.RecordVOI;

public class CandidateVO implements RecordVOI {
	private long recordID;
	private String candidate;
	@Override
	public long getRecordID() {
		return recordID;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public String getErrorMsg() {
		return null;
	}

	@Override
	public void setRecordID(long recordID) {
		this.recordID = recordID;
	}

	@Override
	public void setErrorMsg(String errorMsg) {
	}
	public CandidateVO(long recordID, String candidate) {
		this.recordID = recordID;
		this.candidate = candidate;
	}
	public String toString() {
		return "{recordId:"+recordID + ", candidate:" + candidate + "}";
	}
}
