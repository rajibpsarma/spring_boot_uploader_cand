package org.bitbucket.rajibpsarma.cand.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bitbucket.rajibpsarma.CandidateConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class LinkedInCandidateThreadManager implements ApplicationRunner {
	private ExecutorService service;
	
	@Autowired
	private LinkedInCandidateBulkUploadThread candidateThread;
	
	public void switchOn() {
		System.out.println("*** Switching on Candidate Thread Manager ...");
		if((service == null) || (service.isShutdown())) {
			service = Executors.newFixedThreadPool(1);
		}
		
		service.execute(candidateThread);
	}
	
	public void switchOff() {
		if(service != null) {
			service.shutdown();
		}
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(CandidateConfig.class);
		LinkedInCandidateThreadManager mgr = ctx.getBean(LinkedInCandidateThreadManager.class, "LinkedInCandidateThreadManager");
		mgr.switchOn();
	}
}
