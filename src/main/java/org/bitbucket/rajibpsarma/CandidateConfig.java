package org.bitbucket.rajibpsarma;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.bitbucket.rajibpsarma")
public class CandidateConfig {
}