# spring_boot_uploader_cand #

It's a Spring Boot App that uses the common code in project 
[spring_boot_uploader_libs](https://bitbucket.org/rajibpsarma/spring_boot_uploader_libs)

### Steps (only testing steps) : ###
* Build the jar using "mvn clean package"
* Build a docker image "uploader_cand" using command "sudo docker build -t uploader_cand ."
* Create a container, publish 8081 port of host to 8080 of container, using "sudo docker run -d -p 8081:8080 --name uploader_cand uploader_cand".
* The REST end point is available at "http://localhost:8081/rest/candidates"
* sudo docker container stop uploader_cand
* sudo docker rm uploader_cand
* sudo docker rmi uploader_cand

#### The following projects are part of a system that works together : ####
* [spring_boot_uploader_libs](https://bitbucket.org/rajibpsarma/spring_boot_uploader_libs)
* [spring_boot_uploader_job](https://bitbucket.org/rajibpsarma/spring_boot_uploader_job)
* [spring_boot_uploader_cand](https://bitbucket.org/rajibpsarma/spring_boot_uploader_cand)
* [uploader-ui](https://bitbucket.org/rajibpsarma/uploader-ui)
