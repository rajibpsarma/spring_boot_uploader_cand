package org.bitbucket.rajibpsarma.cand.thread;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.rajibpsarma.cand.vo.CandidateVO;
import org.bitbucket.rajibpsarma.common.Constants.EntityType;
import org.bitbucket.rajibpsarma.common.RecordVOI;
import org.bitbucket.rajibpsarma.common.thread.LinkedInBulkUploadThread;
import org.springframework.stereotype.Component;

@Component
public class LinkedInCandidateBulkUploadThread extends LinkedInBulkUploadThread {

	@Override
	public EntityType getEntityType() {
		return EntityType.CANDIDATE;
	}

	@Override
	public List<RecordVOI> getNextRecords(long lastEntityId) {
		List<RecordVOI> list = new ArrayList<RecordVOI>();
		
		list.add(new CandidateVO(1, "Candidate 1"));
		list.add(new CandidateVO(2, "Candidate 2"));
		list.add(new CandidateVO(3, "Candidate 3"));
		
		return list;
	}

}
